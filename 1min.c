#include <stdio.h>
#include <wiringPi.h>

int main(void) {
	wiringPiSetup();
	pinMode(0, OUTPUT);
	pinMode(1, OUTPUT);
	pinMode(2, OUTPUT);
	pinMode(3, OUTPUT);
	pinMode(4, OUTPUT);
	pinMode(5, OUTPUT);
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	digitalWrite(0, HIGH);
	digitalWrite(1, HIGH);
	digitalWrite(2, HIGH);
	digitalWrite(3, HIGH);
	digitalWrite(4, HIGH);
	digitalWrite(5, HIGH);
	digitalWrite(6, HIGH);
	digitalWrite(7, HIGH);
	
	//Zeiten
	
	int time = 1 * 60000 ;
	int time2 = 15000;
	//Motor An, Druck Aufbauen
	
	digitalWrite(0, LOW);
	delay(10000);
	
	//Topf
	digitalWrite(1, LOW);
	delay(time2);
	digitalWrite(1, HIGH);
	delay(1000);
	//Pflanzen
	digitalWrite(2, LOW);
	delay(time);
	digitalWrite(2, HIGH);
	delay(1000);
	digitalWrite(3, LOW);
	delay(time);
	digitalWrite(3, HIGH);
	delay(1000);
	//Topf2
	digitalWrite(1, LOW);
        delay(time2);
        digitalWrite(1, HIGH);
        delay(1000);
	//Pflanzen
	digitalWrite(4, LOW);
	delay(time);
	digitalWrite(4, HIGH);
	delay(1000);
	digitalWrite(5, LOW);
	delay(time);
	digitalWrite(5, HIGH);
	delay(1000);
	digitalWrite(6, LOW);
	delay(time);
	digitalWrite(6, HIGH);
	delay(1000);
	//Topf3
	digitalWrite(1, LOW);
        delay(time2);
        digitalWrite(1, HIGH);
        delay(1000);
	//Motor aus
	digitalWrite(0, HIGH);

	}
