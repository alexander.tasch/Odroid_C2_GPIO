#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include<signal.h>
#include<unistd.h>

void sig_handler(int signo)
{
	if (signo == SIGINT)
	printf("received SIGINT\n");
	exit(0);
}

void functionA () {
	printf("This is functionA\n");
}

int main () {
	
	printf("Starting  main program...\n");
	
	if (signal(SIGINT, sig_handler) == SIG_ERR)
	printf("\ncan't catch SIGINT\n");
	int i;

	for(i=0; i<5; i++) {
		printf("Zahl %d\n", i+1);
		delay(2000);
	}

   	printf("Exiting main program...\n");

   	return(0);
}
